# [Blog](https://randomsource.club/)

i know you're using windows and chrome

## [I know you're using Windows and Chrome]

Posted on by Mr T

<div>

Stop it. Use BSD or linux. Because I can see what you are doing on your computer.

Posted in Computing

</div>

## [More people need websites.]

<div>

If you're interested in computers, like me, I think it is actually really fun setting up your own website.

Posted in Uncategorized

</div>

## [What is Monero, and why do I recommend it?]

Posted on by Mr T

![](https://www.getmonero.org/press-kit/symbols/monero-symbol-on-white-480.png)

<div>

If you’ve been interested about crypto, you may or may not know about the big name bitcoin. Bitcoin has been made as an alternate way to pay digitally “privately” and “securely”. Unfortunately, through the paces of time it hasn’t stood up to its claims. Bitcoin can be traced and can be seen by how it […]

Posted in Privacy & Security

</div>

## [We Need More tor Nodes]

Posted on by Ben R

![](http://cdn2.expertreviews.co.uk/sites/expertreviews/files/styles/er_main_wide/public/3/28/tor_logo_0.png?itok=MkJgTUOu)

<div>

Tor only currently has 6000 relays and 2000 bridges. Who knows how many of them can easily be made by the government agencies. Tor is our defense towards censorship. And we can start making more relays and bridges to help decentralize the internet.

Posted in Privacy & Security

</div>

## [What is solar? And why does it matter?]

Posted on by Ben R

![](https://aecom.com/without-limits/wp-content/uploads/2020/06/Solar-panel-and-wind-turbines_AdobeStock_276818517-scaled-e1593182712784.jpeg)

<div>

There are many ways to get electricity, especially from sources of coal and nonrenewable resources. But who knows if we could ever get out of this debt of ever ending energy, that we will one day have to rely on renewable sources, like solar and hydroelectric and wind. Solar is the most budget conscious friendly […]

Posted in Green Tech

</div>

## [Advice for myself on Web hosting]

Posted on by Mr T

![](https://www.oxfordwebstudio.com/user/pages/06.da-li-znate/sta-je-web-hosting/sta-je-web-hosting.jpg)

<div>

I'm thinking one day I might make 'servers' locally rather than using a VPS or web hosting. But the reason why I haven't done that, is because I don't have an ISP that allows me to host my own email/website. Sorry this was a very short blog post. Maybe I should combine this with my [...]

Posted in Web Hosting

</div>

## [How I Made This Website]

Posted on by Mr T

![](https://www.dreamhost.com/blog/wp-content/uploads/2019/07/38c18fc3-d6bc-46dc-abf5-3a2236c25349_DreamHost-web-host-checklist-feat-750x401.jpg)

<div>

This is my first post about websites. How did I make this website? I use Monero signing up for a domain service and a web hosting & VPS service. Monero is a sort of currency I recommend for privacy and security, because it is made to emulate cash but digitally. I use Njalla, and 1984, […]

Posted in Web Hosting

</div>

## <p style="text-align:left;">Archives<span style="float:right;">Categories</span></p>

*   [May 2022]

*   [Computing]
*   [Green Tech]
*   [Privacy & Security]
*   [Web Hosting]

### <p style="text-align:center;">do not copy my website or you will go to jail</p>
